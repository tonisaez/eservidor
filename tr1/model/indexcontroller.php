<?php

require_once(__DIR__.'/../controller/fraseslist.php');

class IndexController{

    private $autor = ['Elkin Arboleda',"Marc ElMago","Andrew Mampie","Toni Yatusa"];
    private $frases = [ 'Elkin Arboleda' => ["Caminante no hay camino al andar", "Caminante se hace camino al andar"],
        'Marc ElMago' => ["Qui totum vult totum perdit","Acta deos numquam mortalia fallunt"],
        'Andrew Mampie' => ["Alea iacta est","Carpe vinum"],
        'Toni Yatusa' => ["Acta non verba","Dulce periculum"]];


    public function randomListAction($ord = 10){
        $ret = array();
        for($i = 0 ; $i < $ord ; $i++){
            $indexa = rand(0, count($this->autor)-1);

            $a = $this->autor[$indexa];

            $indexp = rand(0, count($this->frases[$a])-1);

            $f = $this->frases[$a][$indexp];

            $reta = new fraseslist($a, $f);

            array_push($ret, $reta);
        }
        return $ret;

    }






}
